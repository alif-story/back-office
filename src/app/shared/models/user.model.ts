export enum UserRole {
  Admin = 'Admin',
  Employee = 'user',
}

export interface UserApiInterface {
  _id: string;
  id: string;
  first_name: string;
  last_name: string;
  email: string;
  phoneNumber: string;
  password: string;
  role: UserRole;
}

interface UserInterface {
  _id: string;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  role: UserRole;
  password: string;
}

export class User implements UserInterface {
  _id: string;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  role: UserRole;
  password: string;

  constructor(user: UserApiInterface) {
    this._id = user._id;
    this.firstName = user.first_name;
    this.lastName = user.last_name;
    this.email = user.email;
    this.phoneNumber = user.phoneNumber;
    this.role = user.role;
    this.password = user.password;
  }
}
