import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ExampleInterceptorInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
     request = request.clone({
      setHeaders: {
        'Content-Type' : 'application/json; charset=utf-8',
        'Accept'       : 'application/json',
        'Cookie': `accessToken ${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTI4Njk0YzQ1ZmIyZDc0NjIzZWYyZWIiLCJlbWFpbCI6Im1hZGllLmFuZHlAZXhhbXBsZS5jb20iLCJmaXJzdE5hbWUiOiJNYWRpZSIsImxhc3ROYW1lIjoiQW5keSIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjp0cnVlLCJyb2xlIjoidXNlciIsImNhcnQiOltdLCJ3aXNobGlzdCI6W10sInZlcmlmaWNhdGlvbkNvZGUiOiI0dk5xRmxQYVhHbkdDdkZMdXdpczgiLCJjcmVhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ2OjUyLjc0MFoiLCJ1cGRhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ3OjU4LjQ4NFoiLCJfX3YiOjAsInNlc3Npb24iOiI2NTJhZjQ3NzdjODUyY2FlMGE1ZGIxYTkiLCJpYXQiOjE2OTczMTM5MTYsImV4cCI6MTY5NzMxNDgxNn0.m5A9skZPOv89qnBXAlyZ4WBpwyChrXBvztRIj0ZSYETM4dTem4D1-l0EIlYeHy1QvOzPeRZpcDR2GOZ_rGbmhVAfF14SZ1nLTkdcjXaPqkknQ3gZvfmcO4oXL5OtmZFOUF_xx45gPldkLAOG4aJFP_ltgE3HTyoQknHEXQdAsuZ_RXsD2VjAdwNx_h71M6vG5Bd2FJbMzqVFcoORd0THvNSZmCVCAZJVUw2C_i9OrDf_WjS0YAOCJte4gr1fcqswwiaXSc9HmCW-WGhNB5Iuh2Cv24V3Oy0y0PGr5EDiL3Jb7I6pgzkuK1rFASRK139b3Z5tHUfg4Ba-H4unsNG5Dw"}`,
      },
    });
    return next.handle(request);
  }
}
