import {Injectable} from '@angular/core';

export const localStorageKey = '__app_storage__';
export const RTOKEN_KEY = 'token';
export const ATOKEN_KEY = 'token'

@Injectable()
export class LocalStorageFacade {
  public static setSavedState(state: any, key: string = localStorageKey) {
    localStorage.setItem(key, JSON.stringify(state));
  }

  public static returnSavedState(key: string = localStorageKey) {
    return JSON.parse(localStorage.getItem(key));
  }

  public setAuthToken(token: string) {
    localStorage.setItem(ATOKEN_KEY, token);
  }

  public getAuthToken(): string {
    return localStorage.getItem(ATOKEN_KEY);
  }
  public setRefreshToken(token: string) {
    localStorage.setItem(RTOKEN_KEY, token);
  }

  public getRefreshToken(): string {
    return localStorage.getItem(RTOKEN_KEY);
  }

  public clearAuthData(): void {
    localStorage.clear();
  }
}
