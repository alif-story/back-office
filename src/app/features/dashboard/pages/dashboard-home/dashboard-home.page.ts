import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthGuardService } from 'src/app/core/guards';
import { LocalStorageFacade } from 'src/app/core/services';

@Component({
  templateUrl: './dashboard-home.page.html',
  styleUrls: ['./dashboard-home.page.scss'],
})

export class DashboardHomePageComponent implements OnInit {
  url!: string;
  loginRequest: any;
  constructor(private http: HttpClient, private authService: AuthGuardService
    ,private localStorageFacade:LocalStorageFacade) {

    this.url = 'http://localhost:3000/api/';
    this.loginRequest = {
      email: "madie.andy@example.com",
      password: "password"
    }

  
  }
  ngOnInit(): void {
    this.getusers();
    this.test();
    throw new Error('Method not implemented.');
  }

  test() {
    this.http.post<any>(this.url+'sessions', this.loginRequest, {
      headers: new HttpHeaders({
        'Cookie': `accessToken ${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTI4Njk0YzQ1ZmIyZDc0NjIzZWYyZWIiLCJlbWFpbCI6Im1hZGllLmFuZHlAZXhhbXBsZS5jb20iLCJmaXJzdE5hbWUiOiJNYWRpZSIsImxhc3ROYW1lIjoiQW5keSIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjp0cnVlLCJyb2xlIjoidXNlciIsImNhcnQiOltdLCJ3aXNobGlzdCI6W10sInZlcmlmaWNhdGlvbkNvZGUiOiI0dk5xRmxQYVhHbkdDdkZMdXdpczgiLCJjcmVhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ2OjUyLjc0MFoiLCJ1cGRhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ3OjU4LjQ4NFoiLCJfX3YiOjAsInNlc3Npb24iOiI2NTJhZjQ3NzdjODUyY2FlMGE1ZGIxYTkiLCJpYXQiOjE2OTczMTM5MTYsImV4cCI6MTY5NzMxNDgxNn0.m5A9skZPOv89qnBXAlyZ4WBpwyChrXBvztRIj0ZSYETM4dTem4D1-l0EIlYeHy1QvOzPeRZpcDR2GOZ_rGbmhVAfF14SZ1nLTkdcjXaPqkknQ3gZvfmcO4oXL5OtmZFOUF_xx45gPldkLAOG4aJFP_ltgE3HTyoQknHEXQdAsuZ_RXsD2VjAdwNx_h71M6vG5Bd2FJbMzqVFcoORd0THvNSZmCVCAZJVUw2C_i9OrDf_WjS0YAOCJte4gr1fcqswwiaXSc9HmCW-WGhNB5Iuh2Cv24V3Oy0y0PGr5EDiL3Jb7I6pgzkuK1rFASRK139b3Z5tHUfg4Ba-H4unsNG5Dw"}`,
      }),

    }).subscribe(
      response => {
        console.log(response);
        this.localStorageFacade.setAuthToken(response.message.accessToken)
        this.localStorageFacade.setRefreshToken(response.message.refreshToken)
        this.localStorageFacade.getRefreshToken();
        console.log(this.localStorageFacade.getRefreshToken());
        this.http.get<any>(this.url+'users/me', {
          headers: new HttpHeaders({
            'Cookie': `accessToken ${response.message.accessToken}`,
            'Authorization': `Bearer ${response.message.accessToken}`,
            'x-refresh': `${this.localStorageFacade.getRefreshToken()}`
    
          }),
    
        }).subscribe(
          response => {
            console.log(response);
          });
      }); 
  }

  getusers(){
    this.http.get<any>(this.url+'users/me', {
      headers: new HttpHeaders({
        'Cookie': `accessToken ${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTI4Njk0YzQ1ZmIyZDc0NjIzZWYyZWIiLCJlbWFpbCI6Im1hZGllLmFuZHlAZXhhbXBsZS5jb20iLCJmaXJzdE5hbWUiOiJNYWRpZSIsImxhc3ROYW1lIjoiQW5keSIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjp0cnVlLCJyb2xlIjoidXNlciIsImNhcnQiOltdLCJ3aXNobGlzdCI6W10sInZlcmlmaWNhdGlvbkNvZGUiOiI0dk5xRmxQYVhHbkdDdkZMdXdpczgiLCJjcmVhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ2OjUyLjc0MFoiLCJ1cGRhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ3OjU4LjQ4NFoiLCJfX3YiOjAsInNlc3Npb24iOiI2NTJhZjQ3NzdjODUyY2FlMGE1ZGIxYTkiLCJpYXQiOjE2OTczMTM5MTYsImV4cCI6MTY5NzMxNDgxNn0.m5A9skZPOv89qnBXAlyZ4WBpwyChrXBvztRIj0ZSYETM4dTem4D1-l0EIlYeHy1QvOzPeRZpcDR2GOZ_rGbmhVAfF14SZ1nLTkdcjXaPqkknQ3gZvfmcO4oXL5OtmZFOUF_xx45gPldkLAOG4aJFP_ltgE3HTyoQknHEXQdAsuZ_RXsD2VjAdwNx_h71M6vG5Bd2FJbMzqVFcoORd0THvNSZmCVCAZJVUw2C_i9OrDf_WjS0YAOCJte4gr1fcqswwiaXSc9HmCW-WGhNB5Iuh2Cv24V3Oy0y0PGr5EDiL3Jb7I6pgzkuK1rFASRK139b3Z5tHUfg4Ba-H4unsNG5Dw"}`,
        'Authorization': `Bearer ${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTI4Njk0YzQ1ZmIyZDc0NjIzZWYyZWIiLCJlbWFpbCI6Im1hZGllLmFuZHlAZXhhbXBsZS5jb20iLCJmaXJzdE5hbWUiOiJNYWRpZSIsImxhc3ROYW1lIjoiQW5keSIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjp0cnVlLCJyb2xlIjoidXNlciIsImNhcnQiOltdLCJ3aXNobGlzdCI6W10sInZlcmlmaWNhdGlvbkNvZGUiOiI0dk5xRmxQYVhHbkdDdkZMdXdpczgiLCJjcmVhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ2OjUyLjc0MFoiLCJ1cGRhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ3OjU4LjQ4NFoiLCJfX3YiOjAsInNlc3Npb24iOiI2NTJhZjQ3NzdjODUyY2FlMGE1ZGIxYTkiLCJpYXQiOjE2OTczMTM5MTYsImV4cCI6MTY5NzMxNDgxNn0.m5A9skZPOv89qnBXAlyZ4WBpwyChrXBvztRIj0ZSYETM4dTem4D1-l0EIlYeHy1QvOzPeRZpcDR2GOZ_rGbmhVAfF14SZ1nLTkdcjXaPqkknQ3gZvfmcO4oXL5OtmZFOUF_xx45gPldkLAOG4aJFP_ltgE3HTyoQknHEXQdAsuZ_RXsD2VjAdwNx_h71M6vG5Bd2FJbMzqVFcoORd0THvNSZmCVCAZJVUw2C_i9OrDf_WjS0YAOCJte4gr1fcqswwiaXSc9HmCW-WGhNB5Iuh2Cv24V3Oy0y0PGr5EDiL3Jb7I6pgzkuK1rFASRK139b3Z5tHUfg4Ba-H4unsNG5Dw"}`,
        'x-refresh': `${this.localStorageFacade.getRefreshToken()}`

      }),

    }).subscribe(
      response => {
        console.log(response);
      }); 

  }


}
