import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { User } from '../../../../shared/models/user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocalStorageFacade } from 'src/app/core/services';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class UsersListComponent implements OnInit, OnDestroy {
  @Input() users: Observable<User[]>;
  @Input() user: User[];
  @Input() isLoading = false;
  @Output() deleteUser: EventEmitter<User> = new EventEmitter<User>();
  @Output() editUser: EventEmitter<User> = new EventEmitter<User>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  dataSource: MatTableDataSource<User> = new MatTableDataSource<User>();
  displayedColumns = [
    'firstName',
    'lastName',
    'email',
    'phoneNumber',
    'role',
    'actions',
  ];

  private usersSubscription: Subscription;
  url!: string;
  loginRequest: any;
  constructor(private http: HttpClient,private localStorageFacade:LocalStorageFacade) {
    this.url = 'http://localhost:3000/api/';
    this.loginRequest = {
      email: "madie.andy@example.com",
      password: "password"
    }
  }
  ngOnInit(): void {
    console.log(this.localStorageFacade.getRefreshToken());
    this.dataSource = new MatTableDataSource<User>(this.user);
    this.dataSource.paginator = this.paginator;
  }

  ngOnDestroy(): void {
    // this.usersSubscription.unsubscribe();
  }
 

  deleteThisUser(element: any): void {
    console.log(element._id)
    this.http.delete<any>(this.url + 'users/delete/' + element._id, {
      headers: new HttpHeaders({
        'Cookie': `accessToken ${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTJlY2MzNjE2ZmMwZDg4MzExYmFjZTUiLCJlbWFpbCI6Im5hYmlsQGdtYWlsLmNvbSIsImZpcnN0TmFtZSI6Im5hYmlsIiwibGFzdE5hbWUiOiJobiIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjpmYWxzZSwicm9sZSI6InVzZXIiLCJjYXJ0IjpbXSwid2lzaGxpc3QiOltdLCJ2ZXJpZmljYXRpb25Db2RlIjoiM3ZjRndYQS15bzFuOHJHTFhPUWlRIiwiY3JlYXRlZEF0IjoiMjAyMy0xMC0xN1QxODowMjozMC43MjhaIiwidXBkYXRlZEF0IjoiMjAyMy0xMC0xN1QxODowMjozMC43MjhaIiwiX192IjowLCJzZXNzaW9uIjoiNjUyZWNkNGIxNmZjMGQ4ODMxMWJhY2VlIiwiaWF0IjoxNjk3NTY2MDI3LCJleHAiOjE2OTc1NjY5Mjd9.OwM_h73TPac7YGvfzoZ57Ad50Pi9yTqp5QL4C0sOaqhTrqsUifbAZchKjWVuIAfqYGRF7Vf3-dDzwd5VG9Jy3oOVYuliXU-wdee5u4FH6UVpEckt5s9fPvBcdCXoRJtxbwHdehZokyWRdMvhr4j4gk08Bgx515ZUIcpZLqXS6V24K6SW2XWL98H8XSQg_4f0N1tlgx0UWDoAKPceF-PpDOg9Qbdurb3R67fizgKOM3uDLk8ZRItWP-r2TgFeS9nsd-a_dc-L0oEcbKfn_SfETZsOxqGHGLPboTV1w3QqU85lO-2JJZk8FiO14kQX8PDZt7UPHCDl9f_hK7ZwYCvU7Q"}`,
        'Authorization': `Bearer ${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTJlY2MzNjE2ZmMwZDg4MzExYmFjZTUiLCJlbWFpbCI6Im5hYmlsQGdtYWlsLmNvbSIsImZpcnN0TmFtZSI6Im5hYmlsIiwibGFzdE5hbWUiOiJobiIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjpmYWxzZSwicm9sZSI6InVzZXIiLCJjYXJ0IjpbXSwid2lzaGxpc3QiOltdLCJ2ZXJpZmljYXRpb25Db2RlIjoiM3ZjRndYQS15bzFuOHJHTFhPUWlRIiwiY3JlYXRlZEF0IjoiMjAyMy0xMC0xN1QxODowMjozMC43MjhaIiwidXBkYXRlZEF0IjoiMjAyMy0xMC0xN1QxODowMjozMC43MjhaIiwiX192IjowLCJzZXNzaW9uIjoiNjUyZWNkNGIxNmZjMGQ4ODMxMWJhY2VlIiwiaWF0IjoxNjk3NTY2MDI3LCJleHAiOjE2OTc1NjY5Mjd9.OwM_h73TPac7YGvfzoZ57Ad50Pi9yTqp5QL4C0sOaqhTrqsUifbAZchKjWVuIAfqYGRF7Vf3-dDzwd5VG9Jy3oOVYuliXU-wdee5u4FH6UVpEckt5s9fPvBcdCXoRJtxbwHdehZokyWRdMvhr4j4gk08Bgx515ZUIcpZLqXS6V24K6SW2XWL98H8XSQg_4f0N1tlgx0UWDoAKPceF-PpDOg9Qbdurb3R67fizgKOM3uDLk8ZRItWP-r2TgFeS9nsd-a_dc-L0oEcbKfn_SfETZsOxqGHGLPboTV1w3QqU85lO-2JJZk8FiO14kQX8PDZt7UPHCDl9f_hK7ZwYCvU7Q"}`,
        'x-refresh': `${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTJlY2MzNjE2ZmMwZDg4MzExYmFjZTUiLCJlbWFpbCI6Im5hYmlsQGdtYWlsLmNvbSIsImZpcnN0TmFtZSI6Im5hYmlsIiwibGFzdE5hbWUiOiJobiIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjpmYWxzZSwicm9sZSI6InVzZXIiLCJjYXJ0IjpbXSwid2lzaGxpc3QiOltdLCJ2ZXJpZmljYXRpb25Db2RlIjoiM3ZjRndYQS15bzFuOHJHTFhPUWlRIiwiY3JlYXRlZEF0IjoiMjAyMy0xMC0xN1QxODowMjozMC43MjhaIiwidXBkYXRlZEF0IjoiMjAyMy0xMC0xN1QxODowMjozMC43MjhaIiwiX192IjowLCJzZXNzaW9uIjoiNjUyZWNkNGIxNmZjMGQ4ODMxMWJhY2VlIiwiaWF0IjoxNjk3NTY2MDI3LCJleHAiOjE3MjkxMjM2Mjd9.Fp9J88FOKvKG19-JUO9Q8ah6RubB5eWx-0TBkGTjILSESn2MEKLxvTkp6OeL52QZokZPIhovI6MI0viG5-zUQZXK73zUG9O0PHTDe99tRW_Z1bf-_9ir_LjtmIwNB0giCPdR2ecnHkCcc4PNODqgtbKm0IsGCtetTEXydNt1AQ_VEY9BqRjtdqaEmS-D00HOuRptG8XbH8ThdkZGTJhVZEMx2xoGyf5n4sebtmQSx14dwrXd9aHd7uJ-M0ItltVXAD3nZTjbVe7wZdOCwaMinlj4HwNSBwNwjzUstGv3Z06vmyLCaW4k4yfRklH48kEvQBOU7XjB7cSS5sik9Zxjkg"}`

      }),

    })
      .subscribe(() => 'Delete successful');
  }

  addNewUser(element: any): void {
    console.log(element._id)
    this.http.delete<any>(this.url + 'users/delete/' + element._id, {
      headers: new HttpHeaders({
        'Cookie': `accessToken ${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTJlY2MzNjE2ZmMwZDg4MzExYmFjZTUiLCJlbWFpbCI6Im5hYmlsQGdtYWlsLmNvbSIsImZpcnN0TmFtZSI6Im5hYmlsIiwibGFzdE5hbWUiOiJobiIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjpmYWxzZSwicm9sZSI6InVzZXIiLCJjYXJ0IjpbXSwid2lzaGxpc3QiOltdLCJ2ZXJpZmljYXRpb25Db2RlIjoiM3ZjRndYQS15bzFuOHJHTFhPUWlRIiwiY3JlYXRlZEF0IjoiMjAyMy0xMC0xN1QxODowMjozMC43MjhaIiwidXBkYXRlZEF0IjoiMjAyMy0xMC0xN1QxODowMjozMC43MjhaIiwiX192IjowLCJzZXNzaW9uIjoiNjUyZWNkNGIxNmZjMGQ4ODMxMWJhY2VlIiwiaWF0IjoxNjk3NTY2MDI3LCJleHAiOjE2OTc1NjY5Mjd9.OwM_h73TPac7YGvfzoZ57Ad50Pi9yTqp5QL4C0sOaqhTrqsUifbAZchKjWVuIAfqYGRF7Vf3-dDzwd5VG9Jy3oOVYuliXU-wdee5u4FH6UVpEckt5s9fPvBcdCXoRJtxbwHdehZokyWRdMvhr4j4gk08Bgx515ZUIcpZLqXS6V24K6SW2XWL98H8XSQg_4f0N1tlgx0UWDoAKPceF-PpDOg9Qbdurb3R67fizgKOM3uDLk8ZRItWP-r2TgFeS9nsd-a_dc-L0oEcbKfn_SfETZsOxqGHGLPboTV1w3QqU85lO-2JJZk8FiO14kQX8PDZt7UPHCDl9f_hK7ZwYCvU7Q"}`,
        'Authorization': `Bearer ${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTJlY2MzNjE2ZmMwZDg4MzExYmFjZTUiLCJlbWFpbCI6Im5hYmlsQGdtYWlsLmNvbSIsImZpcnN0TmFtZSI6Im5hYmlsIiwibGFzdE5hbWUiOiJobiIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjpmYWxzZSwicm9sZSI6InVzZXIiLCJjYXJ0IjpbXSwid2lzaGxpc3QiOltdLCJ2ZXJpZmljYXRpb25Db2RlIjoiM3ZjRndYQS15bzFuOHJHTFhPUWlRIiwiY3JlYXRlZEF0IjoiMjAyMy0xMC0xN1QxODowMjozMC43MjhaIiwidXBkYXRlZEF0IjoiMjAyMy0xMC0xN1QxODowMjozMC43MjhaIiwiX192IjowLCJzZXNzaW9uIjoiNjUyZWNkNGIxNmZjMGQ4ODMxMWJhY2VlIiwiaWF0IjoxNjk3NTY2MDI3LCJleHAiOjE2OTc1NjY5Mjd9.OwM_h73TPac7YGvfzoZ57Ad50Pi9yTqp5QL4C0sOaqhTrqsUifbAZchKjWVuIAfqYGRF7Vf3-dDzwd5VG9Jy3oOVYuliXU-wdee5u4FH6UVpEckt5s9fPvBcdCXoRJtxbwHdehZokyWRdMvhr4j4gk08Bgx515ZUIcpZLqXS6V24K6SW2XWL98H8XSQg_4f0N1tlgx0UWDoAKPceF-PpDOg9Qbdurb3R67fizgKOM3uDLk8ZRItWP-r2TgFeS9nsd-a_dc-L0oEcbKfn_SfETZsOxqGHGLPboTV1w3QqU85lO-2JJZk8FiO14kQX8PDZt7UPHCDl9f_hK7ZwYCvU7Q"}`,
        'x-refresh': `${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTJlY2MzNjE2ZmMwZDg4MzExYmFjZTUiLCJlbWFpbCI6Im5hYmlsQGdtYWlsLmNvbSIsImZpcnN0TmFtZSI6Im5hYmlsIiwibGFzdE5hbWUiOiJobiIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjpmYWxzZSwicm9sZSI6InVzZXIiLCJjYXJ0IjpbXSwid2lzaGxpc3QiOltdLCJ2ZXJpZmljYXRpb25Db2RlIjoiM3ZjRndYQS15bzFuOHJHTFhPUWlRIiwiY3JlYXRlZEF0IjoiMjAyMy0xMC0xN1QxODowMjozMC43MjhaIiwidXBkYXRlZEF0IjoiMjAyMy0xMC0xN1QxODowMjozMC43MjhaIiwiX192IjowLCJzZXNzaW9uIjoiNjUyZWNkNGIxNmZjMGQ4ODMxMWJhY2VlIiwiaWF0IjoxNjk3NTY2MDI3LCJleHAiOjE3MjkxMjM2Mjd9.Fp9J88FOKvKG19-JUO9Q8ah6RubB5eWx-0TBkGTjILSESn2MEKLxvTkp6OeL52QZokZPIhovI6MI0viG5-zUQZXK73zUG9O0PHTDe99tRW_Z1bf-_9ir_LjtmIwNB0giCPdR2ecnHkCcc4PNODqgtbKm0IsGCtetTEXydNt1AQ_VEY9BqRjtdqaEmS-D00HOuRptG8XbH8ThdkZGTJhVZEMx2xoGyf5n4sebtmQSx14dwrXd9aHd7uJ-M0ItltVXAD3nZTjbVe7wZdOCwaMinlj4HwNSBwNwjzUstGv3Z06vmyLCaW4k4yfRklH48kEvQBOU7XjB7cSS5sik9Zxjkg"}`

      }),

    })
      .subscribe(() => 'Delete successful');
  }

}
