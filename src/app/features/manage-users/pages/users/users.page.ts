import {Component, OnInit} from '@angular/core';
import {UserFacadeService} from '../../services/user-facade.service';
import {Observable} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmationDialogComponent} from '../../../../shared/components';
import {User, UserApiInterface} from '../../../../shared/models/user.model';
import {UserDialogComponent} from '../../containers';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocalStorageFacade } from 'src/app/core/services';
import { OnInitEffects } from '@ngrx/effects';

@Component({
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})

export class UsersPageComponent implements OnInit {

  allUsers$: Observable<User[]>;
  isLoading = false;
  url!: string;
  userTosend: User[];
  constructor(private http: HttpClient,private localStorageFacade:LocalStorageFacade,private readonly usersFacade: UserFacadeService, public dialog: MatDialog) {
    this.allUsers$ = this.usersFacade.getAll();
    this.url = 'http://localhost:3000/api/';
 
  }
  ngOnInit(): void {
this.getusers();
    throw new Error('Method not implemented.');
  }

  onDeleteUser(user: User): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe(res => {
      if (res.confirmed) {
        this.isLoading = true;
        this.usersFacade.deleteUser(user._id)
          .subscribe(() => {
            this.isLoading = false;
          });
      }
    });
  }

  onCreateUser(): void {
    const dialogRef = this.dialog.open(UserDialogComponent, {
      width: '600px',
    });

    dialogRef.afterClosed().subscribe(res => {
      if (res.confirmed) {
        console.log(res.data)
        this.http.post<any>(this.url + 'users',res.data, {
          headers: new HttpHeaders({
            'Cookie': `accessToken ${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTI4Njk0YzQ1ZmIyZDc0NjIzZWYyZWIiLCJlbWFpbCI6Im1hZGllLmFuZHlAZXhhbXBsZS5jb20iLCJmaXJzdE5hbWUiOiJNYWRpZSIsImxhc3ROYW1lIjoiQW5keSIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjp0cnVlLCJyb2xlIjoidXNlciIsImNhcnQiOltdLCJ3aXNobGlzdCI6W10sInZlcmlmaWNhdGlvbkNvZGUiOiI0dk5xRmxQYVhHbkdDdkZMdXdpczgiLCJjcmVhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ2OjUyLjc0MFoiLCJ1cGRhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ3OjU4LjQ4NFoiLCJfX3YiOjAsInNlc3Npb24iOiI2NTJhZjQ3NzdjODUyY2FlMGE1ZGIxYTkiLCJpYXQiOjE2OTczMTM5MTYsImV4cCI6MTY5NzMxNDgxNn0.m5A9skZPOv89qnBXAlyZ4WBpwyChrXBvztRIj0ZSYETM4dTem4D1-l0EIlYeHy1QvOzPeRZpcDR2GOZ_rGbmhVAfF14SZ1nLTkdcjXaPqkknQ3gZvfmcO4oXL5OtmZFOUF_xx45gPldkLAOG4aJFP_ltgE3HTyoQknHEXQdAsuZ_RXsD2VjAdwNx_h71M6vG5Bd2FJbMzqVFcoORd0THvNSZmCVCAZJVUw2C_i9OrDf_WjS0YAOCJte4gr1fcqswwiaXSc9HmCW-WGhNB5Iuh2Cv24V3Oy0y0PGr5EDiL3Jb7I6pgzkuK1rFASRK139b3Z5tHUfg4Ba-H4unsNG5Dw"}`,
            'Authorization': `Bearer ${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTI4Njk0YzQ1ZmIyZDc0NjIzZWYyZWIiLCJlbWFpbCI6Im1hZGllLmFuZHlAZXhhbXBsZS5jb20iLCJmaXJzdE5hbWUiOiJNYWRpZSIsImxhc3ROYW1lIjoiQW5keSIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjp0cnVlLCJyb2xlIjoidXNlciIsImNhcnQiOltdLCJ3aXNobGlzdCI6W10sInZlcmlmaWNhdGlvbkNvZGUiOiI0dk5xRmxQYVhHbkdDdkZMdXdpczgiLCJjcmVhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ2OjUyLjc0MFoiLCJ1cGRhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ3OjU4LjQ4NFoiLCJfX3YiOjAsInNlc3Npb24iOiI2NTJhZjQ3NzdjODUyY2FlMGE1ZGIxYTkiLCJpYXQiOjE2OTczMTM5MTYsImV4cCI6MTY5NzMxNDgxNn0.m5A9skZPOv89qnBXAlyZ4WBpwyChrXBvztRIj0ZSYETM4dTem4D1-l0EIlYeHy1QvOzPeRZpcDR2GOZ_rGbmhVAfF14SZ1nLTkdcjXaPqkknQ3gZvfmcO4oXL5OtmZFOUF_xx45gPldkLAOG4aJFP_ltgE3HTyoQknHEXQdAsuZ_RXsD2VjAdwNx_h71M6vG5Bd2FJbMzqVFcoORd0THvNSZmCVCAZJVUw2C_i9OrDf_WjS0YAOCJte4gr1fcqswwiaXSc9HmCW-WGhNB5Iuh2Cv24V3Oy0y0PGr5EDiL3Jb7I6pgzkuK1rFASRK139b3Z5tHUfg4Ba-H4unsNG5Dw"}`,
            'x-refresh': `${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTI4Njk0YzQ1ZmIyZDc0NjIzZWYyZWIiLCJlbWFpbCI6Im1hZGllLmFuZHlAZXhhbXBsZS5jb20iLCJmaXJzdE5hbWUiOiJNYWRpZSIsImxhc3ROYW1lIjoiQW5keSIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjp0cnVlLCJyb2xlIjoidXNlciIsImNhcnQiOltdLCJ3aXNobGlzdCI6W10sInZlcmlmaWNhdGlvbkNvZGUiOiI0dk5xRmxQYVhHbkdDdkZMdXdpczgiLCJjcmVhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ2OjUyLjc0MFoiLCJ1cGRhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ3OjU4LjQ4NFoiLCJfX3YiOjAsInNlc3Npb24iOiI2NTQ1MjdjN2IyNTA3ODY1NTMwMDI3ZTkiLCJpYXQiOjE2OTkwMzA5ODMsImV4cCI6MTczMDU4ODU4M30.EhLxrsmZDSuuvd37e9roEaPd1eV93gzkPAIFXEihbM-Tv7BWh0_Ykcu87BNJRYbINyr3iJg4RuA5cPSabv9RrtFGqGVcbRdte5owmZq7prDMrUGkquTo446a_GPQAff1XOLXi8vFGol7STj0c2oAm6-NG4CUq4Szpb4gBEGXIFFYuF1GKpfhLbWJ5gwWJ01i9WBjcn18aeQ68OOW15P1CYr5xQIevaCht_IVjHAviAyqq2JTvmAMClVw4nrBvFCYxN4QiarX88YvWAtF8OjdBBcU4AtjDP_zs_XytZg-iPcGNUW5VqqCkAABEsou8-ayanivZd07WGxMGu04RVKseA"}`
    
          }),
    
        }).subscribe(
          (response: any) => {
            this.userTosend.push(res.data)
    
            console.log(response);
          });
      }
    });
  }
  getusers() {
    this.http.get<any>(this.url + 'users/me', {
      headers: new HttpHeaders({
        'Cookie': `accessToken ${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTI4Njk0YzQ1ZmIyZDc0NjIzZWYyZWIiLCJlbWFpbCI6Im1hZGllLmFuZHlAZXhhbXBsZS5jb20iLCJmaXJzdE5hbWUiOiJNYWRpZSIsImxhc3ROYW1lIjoiQW5keSIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjp0cnVlLCJyb2xlIjoidXNlciIsImNhcnQiOltdLCJ3aXNobGlzdCI6W10sInZlcmlmaWNhdGlvbkNvZGUiOiI0dk5xRmxQYVhHbkdDdkZMdXdpczgiLCJjcmVhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ2OjUyLjc0MFoiLCJ1cGRhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ3OjU4LjQ4NFoiLCJfX3YiOjAsInNlc3Npb24iOiI2NTJhZjQ3NzdjODUyY2FlMGE1ZGIxYTkiLCJpYXQiOjE2OTczMTM5MTYsImV4cCI6MTY5NzMxNDgxNn0.m5A9skZPOv89qnBXAlyZ4WBpwyChrXBvztRIj0ZSYETM4dTem4D1-l0EIlYeHy1QvOzPeRZpcDR2GOZ_rGbmhVAfF14SZ1nLTkdcjXaPqkknQ3gZvfmcO4oXL5OtmZFOUF_xx45gPldkLAOG4aJFP_ltgE3HTyoQknHEXQdAsuZ_RXsD2VjAdwNx_h71M6vG5Bd2FJbMzqVFcoORd0THvNSZmCVCAZJVUw2C_i9OrDf_WjS0YAOCJte4gr1fcqswwiaXSc9HmCW-WGhNB5Iuh2Cv24V3Oy0y0PGr5EDiL3Jb7I6pgzkuK1rFASRK139b3Z5tHUfg4Ba-H4unsNG5Dw"}`,
        'Authorization': `Bearer ${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTI4Njk0YzQ1ZmIyZDc0NjIzZWYyZWIiLCJlbWFpbCI6Im1hZGllLmFuZHlAZXhhbXBsZS5jb20iLCJmaXJzdE5hbWUiOiJNYWRpZSIsImxhc3ROYW1lIjoiQW5keSIsImFkcmVzc2UiOiJmdmZkc2Rmc2RzZHNkcyIsInBob25lTnVtYmVyIjoyMjI1NDg4NDgsInZlcmlmaWVkIjp0cnVlLCJyb2xlIjoidXNlciIsImNhcnQiOltdLCJ3aXNobGlzdCI6W10sInZlcmlmaWNhdGlvbkNvZGUiOiI0dk5xRmxQYVhHbkdDdkZMdXdpczgiLCJjcmVhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ2OjUyLjc0MFoiLCJ1cGRhdGVkQXQiOiIyMDIzLTEwLTEyVDIxOjQ3OjU4LjQ4NFoiLCJfX3YiOjAsInNlc3Npb24iOiI2NTJhZjQ3NzdjODUyY2FlMGE1ZGIxYTkiLCJpYXQiOjE2OTczMTM5MTYsImV4cCI6MTY5NzMxNDgxNn0.m5A9skZPOv89qnBXAlyZ4WBpwyChrXBvztRIj0ZSYETM4dTem4D1-l0EIlYeHy1QvOzPeRZpcDR2GOZ_rGbmhVAfF14SZ1nLTkdcjXaPqkknQ3gZvfmcO4oXL5OtmZFOUF_xx45gPldkLAOG4aJFP_ltgE3HTyoQknHEXQdAsuZ_RXsD2VjAdwNx_h71M6vG5Bd2FJbMzqVFcoORd0THvNSZmCVCAZJVUw2C_i9OrDf_WjS0YAOCJte4gr1fcqswwiaXSc9HmCW-WGhNB5Iuh2Cv24V3Oy0y0PGr5EDiL3Jb7I6pgzkuK1rFASRK139b3Z5tHUfg4Ba-H4unsNG5Dw"}`,
        'x-refresh': `${this.localStorageFacade.getRefreshToken()}`

      }),

    }).subscribe(
      (response: any) => {
        const userA: User = response.message;
        const user: User[] = [];
        user.push(userA);
        this.userTosend=user
        console.log(userA);
      });

  }
  onEditUserInfo(user: User): void {
    const dialogRef = this.dialog.open(UserDialogComponent, {
      width: '600px',
      data: user,
    });

    dialogRef.afterClosed().subscribe(res => {
      if (res.confirmed) {
        const data: Partial<UserApiInterface> = {
          first_name: res.data.firstName,
          last_name: res.data.lastName,
          email: res.data.email,
          password: res.data.password,
          role: res.data.role,
        };

        this.isLoading = true;
        this.usersFacade.updateUser(user._id, data)
          .subscribe(() => {
            this.isLoading = false;
          });
      }
    });
  }
}
